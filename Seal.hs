{-# LANGUAGE DuplicateRecordFields #-}

module Seal where

import           Data.Bifunctor
import           Data.Bits
import           Data.ByteString (ByteString)
import qualified Data.ByteString as BS
import           Data.Fixed
import           Data.Word
import           Text.Printf

data Config = Config { figNum        :: !Int
                     , maxLen        :: !Double
                     , widthLenRatio :: !Double
                     }

type Point = (Double, Double)

data Pen = Pen { width :: !Double
               , angle :: !Double
               }

data MPPath = EndPath
            | Path { point :: !Point
                   , pen   :: !Pen
                   , dir   :: !Double
                   , next  :: MPPath
                   }

data Atom = Atom { startPt  :: !Point
                 , endPt    :: !Point
                 , startPen :: !Pen
                 , endPen   :: !Pen
                 , dir      :: !Double
                 }

data Stream a = Cons a (Stream a)

asAtoms :: Pen -> MPPath -> [Atom]
asAtoms initialPen path = asAtoms' initialPt initialPen path
  where initialPt = (0,0)
        asAtoms' _ _ EndPath = []
        asAtoms' ept epen (Path { point = pt
                                , pen = pen
                                , dir = dir
                                , next = npath
                                }) = nAtom : asAtoms' pt pen npath
          where nAtom = Atom { startPt = ept
                             , startPen = epen
                             , endPt = pt
                             , endPen = pen
                             , dir = dir
                             }

printPath :: Config -> MPPath -> String
printPath config path = unlines (initMP ++ [setup, draw] ++ closeMP)
  where initMP = [ "prologues := 3;"
                 , "outputtemplate := \"%j%c.eps\";"
                 , "beginfig(" ++ show (figNum config) ++ ");"
                 ]
        closeMP = [ "endfig;"
                  , "end."
                  ]

        numberSrc = let numberSrc' x = Cons x (numberSrc' (x + 1))
                    in numberSrc' (0 :: Int)

        setupPath _ EndPath = []
        setupPath (Cons n ns) (Path { point = (x,y)
                                    , pen = Pen { width = w
                                                , angle = a
                                                }
                                    , dir = dir
                                    , next = npath
                                    }) =
          ( printf "z%d = (%.10f, %.10f);\npenpos%d(%.10f, %.10f);\n"
            n x y n w a
          , printf "..{dir %.10f}z%de" dir n
          ) : setupPath ns npath

        (setup, draw) = bimap concat
                        (\s -> "penstroke origin" ++ concat s ++ ";") $
                        unzip (setupPath numberSrc path)

data CtrlPt = CtrlPt { angle    :: !Word8
                     , len      :: !Word8
                     , dir      :: !Word8
                     , penWidth :: !Word8
                     , penAngle :: !Word8
                     }

data Acc = Acc { oldPt         :: !(Double, Double)
               , previousAngle :: !Double
               }

fromCtrl :: Config -> [CtrlPt] -> MPPath
fromCtrl config ctrls = fromCtrl' config baseAcc ctrls
  where baseAcc = Acc { oldPt = (0,0)
                      , previousAngle = 0
                      }

        fromCtrl' _ _ [] = EndPath
        fromCtrl' config acc (CtrlPt { angle = aw
                                     , len = lw
                                     , dir = dw
                                     , penWidth = pw
                                     , penAngle = paw
                                     } : ctrls) =
          Path { point = (x,y)
               , pen = Pen { width = w
                           , angle = a
                           }
               , dir = dir
               , next = fromCtrl' config nAcc ctrls
               }
          where Acc { oldPt = (ox, oy)
                    , previousAngle = pa
                    } = acc
                nlen = maxLen config * (1 + fromIntegral lw) / 9
                nAngle = pa + ((1 + fromIntegral aw) / 9 * 360) `mod'` 360
                x = ox + sin (- nAngle) * nlen
                y = oy + cos (- nAngle) * nlen
                maxWidth = widthLenRatio config * maxLen config
                w = maxWidth * (1 + fromIntegral pw) / 9
                a = (1 + fromIntegral paw) / 9 * 360
                dir = fromIntegral dw / 8 * 360
                nAcc = Acc { oldPt = (x, y)
                           , previousAngle = nAngle
                           }


ofByte :: Word16 -> (CtrlPt, Bool)
ofByte w = (CtrlPt { angle = fromIntegral angle
                   , len = fromIntegral len
                   , dir = fromIntegral dir
                   , penWidth = fromIntegral penWidth
                   , penAngle = fromIntegral penAngle
                   }, lastBit)
  where w16size = finiteBitSize w
        sliceSize = w16size - 3
        getSlice w = shiftR w sliceSize
        angle = getSlice w
        len = getSlice (shiftL w 3)
        dir = getSlice (shiftL w 6)
        penWidth = getSlice (shiftL w 9)
        penAngle = getSlice (shiftL w 12)
        lastBit = testBit w (w16size - 1)

readBS :: [Word8] -> [CtrlPt]
readBS bs = points
  where (points, supBits) = readBS' ([], 0) bs
        readBS' (pts, acc) [] = (reverse pts, acc)
        readBS' _ [_] = error ""
        readBS' (pts, acc) (x:y:xs) =
          readBS' (ctrl:pts, 2 * acc + if b then 1 else 0) xs
          where nx = fromIntegral x * (fromIntegral (maxBound :: Word8))
                ny = fromIntegral y
                (ctrl, b) = ofByte (nx + ny)


baseConfig :: Config
baseConfig = Config { figNum = 0
                    , maxLen = 1
                    , widthLenRatio = 0.1
                    }

run :: Config -> FilePath -> IO String
run config fp = do
  bs <- BS.readFile fp
  let ctrls = readBS (BS.unpack bs)
      path = fromCtrl config ctrls
  pure $ printPath config path
